//
//  AnalysisHomeView.swift
//  RoutingTest
//
//  Created by Nicolas Lasso on 08/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit

class AnalysisHomeView: UIView {
    let button = prepareButton()
    let timeDepositButton = prepareButton()
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.white
        self.addSubviews()
        self.setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func addSubviews() {
        self.addSubview(button)
        self.addSubview(timeDepositButton)
    }
    
    fileprivate func setConstraints() {
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            button.widthAnchor.constraint(equalToConstant: 100),
            button.heightAnchor.constraint(equalToConstant: 30),
            
            timeDepositButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            timeDepositButton.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 10),
            timeDepositButton.widthAnchor.constraint(equalToConstant: 100),
            timeDepositButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
}

private func prepareButton() -> UIButton {
    let button = UIButton()
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitle("Ir al Loans", for: .normal)
    button.backgroundColor = UIColor.black
    button.setTitleColor(UIColor.white, for: .normal)
    return button
}
