//
//  AnalysisHomeViewController.swift
//  RoutingTest
//
//  Created by Nicolas Lasso on 08/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class AnalysisHomeViewController: UIViewController {
    
    fileprivate let _viewModel: AnalysisHomeViewModel
    fileprivate let _view = AnalysisHomeView()
    fileprivate let _disposeBag = DisposeBag()
    
    init(viewModel: AnalysisHomeViewModel) {
        _viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        bind()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(_view)
        _view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            _view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            _view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            _view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    fileprivate func bind() {
        self._view
            .button
            .rx.tap
            .asDriver()
            .drive(_viewModel.goToLoansTapped)
            .disposed(by: _disposeBag)
        
        self._view.timeDepositButton.setTitle("Ir a plazo fijo", for: .normal)
        
        //TODO: implement in ViewModel the route.
//        self._view
//            .timeDepositButton
//            .rx.tap
//            .asDriver()
//            .drive(_viewModel.goToTimeDepositTapped)
//            .disposed(by: _disposeBag)
        
        self._viewModel
            .goToLoans
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] route in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf._viewModel.routerService.navigate(toRoute: route, fromView: strongSelf)
            })
            .disposed(by: _disposeBag)
        
    }

}
