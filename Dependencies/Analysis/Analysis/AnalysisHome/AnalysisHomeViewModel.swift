//
//  AnalysisHomeViewModel.swift
//  RoutingTest
//
//  Created by Nicolas Lasso on 08/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import LoansRoutesInterface
import RouterServiceInterface
import RxCocoa
import RxSwift

class AnalysisHomeViewModel {
    let routerService: RouterServiceProtocol
    let goToLoansTapped = BehaviorRelay<()>(value: ())
    let goToLoans: Observable<Route>
    
//    let goToTimeDepositTapped = BehaviorRelay<()>(value: ())
//    let goToTimeDeposit: Observable<Route>
    
    fileprivate let _disposeBag = DisposeBag()
    
    init(routerService: RouterServiceProtocol) {
        self.routerService = routerService
        
        goToLoans = goToLoansTapped
            .asObservable()
            .map { _ in LoanDetailsRoute(id: "abc123") }
            .share()
        
        //TODO: implement the Route.
//        goToTimeDeposit = goToTimeDepositTapped
//            .asObservable()
//            .map { _ in }
//            .share()
    }
}
