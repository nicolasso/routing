//
//  AnalysisRouteHandler.swift
//  Analysis
//
//  Created by Nicolas Lasso on 09/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit
import AnalysisRoutesInterface
import RouterServiceInterface

//Dependency resolver
public enum AnalysisHome: Feature {
    public struct Dependencies {
        public let routerService: RouterServiceProtocol
    }
    
    public static var dependenciesInitializer: AnyDependenciesInitializer {
        return AnyDependenciesInitializer(singleDependencyStruct: AnalysisHome.Dependencies.init)
    }
    
    public static func build(dependencies: AnalysisHome.Dependencies, fromRoute: Route?) -> UIViewController {
        let viewModel = AnalysisHomeViewModel(routerService: dependencies.routerService)
        return AnalysisHomeViewController(viewModel: viewModel)
    }
}

public class AnalysisRouteHandler: RouteHandler {
    public var routes: [Route.Type] {
        return [AnalysisHomeRoute.self]
    }
    
    public func destination(of route: Route) -> AnyFeature {
        return AnyFeature(AnalysisHome.self)
    }
    
    public init() { }
}
