//
//  AnalysisHomeRoute.swift
//  AnalysisRoutesInterface
//
//  Created by Nicolas Lasso on 08/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import RouterServiceInterface

public struct AnalysisHomeRoute: Route {
    public static let identifier: String = "analysis_home"
    
    public var userId: String
}
