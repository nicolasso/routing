//
//  LoanDetailsViewController.swift
//  RoutingTest
//
//  Created by Nicolas Lasso on 08/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class LoanDetailsViewController: UIViewController {
    
    fileprivate let _viewModel: LoanDetailsViewModel
    fileprivate let _view = LoanDetailsView()
    
    init(viewModel: LoanDetailsViewModel) {
        _viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        bind()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(_view)
        _view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            _view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            _view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            _view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            _view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    fileprivate func bind() {
        self._view.title.text = "Bienvenido al Prestamo ID: \(self._viewModel.loanId)"
    }

}
