//
//  LoanDetailsViewModel.swift
//  RoutingTest
//
//  Created by Nicolas Lasso on 08/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation

class LoanDetailsViewModel {
    let loanId: String
    let service: LoanServiceProtocol
    init(id: String, service: LoanServiceProtocol) {
        self.loanId = id
        self.service = service
    }
}
