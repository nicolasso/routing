//
//  LoansRouteHandler.swift
//  Loans
//
//  Created by Nicolas Lasso on 09/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit
import LoansRoutesInterface
import RouterServiceInterface

public enum LoanDetails: Feature {
    
    public static var dependenciesInitializer: AnyDependenciesInitializer {
        return AnyDependenciesInitializer(singleDependencyStruct: LoanDetails.Dependencies.init)
    }
    
    public struct Dependencies {
        var loanService: LoanServiceProtocol
    }
    
    public static func build(dependencies: LoanDetails.Dependencies, fromRoute forRoute: Route?) -> UIViewController {
        guard let route = forRoute as? LoansRoutesInterface.LoanDetailsRoute else { return UIViewController() }
        let viewModel = LoanDetailsViewModel(id: route.loanId, service: dependencies.loanService)
        return LoanDetailsViewController(viewModel: viewModel)
    }
}

public class LoansRouteHandler: RouteHandler {
    
    public var routes: [Route.Type] {
        return [LoanDetailsRoute.self]
    }
    
    public func destination(of route: Route) -> AnyFeature {
        return AnyFeature(LoanDetails.self)
    }
    
    public init() { }
}

public class LoanService: LoanServiceProtocol {
    public override init() { }
}

public class LoanServiceProtocol: Dependency { }
