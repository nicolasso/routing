//
//  LoanDetailsRoute.swift
//  LoansRoutesInterface
//
//  Created by Nicolas Lasso on 08/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit
import RouterServiceInterface

public struct LoanDetailsRoute: Route {
    public static let identifier: String = "loan_details"
    
    public var loanId: String
    
    public init(id: String) {
        self.loanId = id
    }
    
}
