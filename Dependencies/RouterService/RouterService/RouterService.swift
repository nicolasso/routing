//
//  RouterService.swift
//  RouterService
//
//  Created by Nicolas Lasso on 10/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit
import RouterServiceInterface

public final class RouterService: Dependency, RouterServiceProtocol {
    var store: StoreInterface
    let failureHandler: () -> Void
    
    private(set) var registeredRoutes = [String: (Route.Type, RouteHandler)]()

    public init(
        store: StoreInterface? = nil,
        failureHandler: @escaping () -> Void = { preconditionFailure() }
    ) {
        self.store = store ?? Store()
        self.failureHandler = failureHandler
        
        register(dependencyFactory: { [unowned self] in
            self
        }, forType: RouterServiceProtocol.self)
    }

    public func register<T>(
        dependencyFactory: @escaping DependencyFactory,
        forType metaType: T.Type
    ) {
        store.register(dependencyFactory, forMetaType: metaType)
    }

    public func register(routeHandler: RouteHandler) {
        routeHandler.routes.forEach {
            registeredRoutes[$0.identifier] = ($0.self, routeHandler)
        }
    }

    public func navigationController<T: Feature>(
        withInitialFeature feature: T.Type
    ) -> UINavigationController {
        let feature = AnyFeature(feature)
        let rootViewController = feature.build(store, nil)
        return UINavigationController(rootViewController: rootViewController)
    }

    
    //To navigate to the route.
    public func navigate(
        toRoute route: Route,
        fromView viewController: UIViewController
    ) {
        guard let handler = handler(forRoute: route) else {
            failureHandler()
            return
        }
        let newVC = handler.destination(
            of: route
            ).build(store, route)
        
        viewController.navigationController?.pushViewController(newVC, animated: true)
    }

    //This will return the Route Handler that belongs to the route.
    func handler(forRoute route: Route) -> RouteHandler? {
        let routeIdentifier = type(of: route).identifier
        return registeredRoutes[routeIdentifier]?.1
    }
}
