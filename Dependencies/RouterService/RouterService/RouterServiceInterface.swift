//
//  RouterSerivice.swift
//  RouterServiceInterface
//
//  Created by Nicolas Lasso on 08/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit

public typealias DependencyFactory = () -> Dependency

public protocol Dependency {}

public protocol Dependencies { }

public protocol Feature {
    
    associatedtype Dependencies
    
    static var dependenciesInitializer: AnyDependenciesInitializer { get }
    
    static func build(dependencies: Dependencies, fromRoute: Route?) -> UIViewController
    
}
