//
//  Store.swift
//  RouterService
//
//  Created by Nicolas Lasso on 10/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import RouterServiceInterface

public final class Store: StoreInterface {

    public var dependencies = NSMapTable<NSString, AnyObject>(
        keyOptions: .strongMemory,
        valueOptions: .weakMemory
    )

    public var dependencyCreators = [String: DependencyFactory]()

    public func get<T>(_ arg: T.Type) -> T? {
        let name = String(describing: arg)
        let object = dependencies.object(forKey: name as NSString)
        if object == nil {
            let dependencyFactory: DependencyFactory = dependencyCreators[name]!
            let dependencie: Dependency = dependencyFactory()
            
            dependencies.setObject(dependencie as AnyObject, forKey: name as NSString)
            return dependencie as? T
        } else {
            return object as? T
        }
    }

    public func register<T>(_ arg: @escaping DependencyFactory, forMetaType metaType: T.Type) {
        let name = String(describing: metaType)
        dependencyCreators[name] = arg
    }
}
