//
//  AnyFeature.swift
//  RouterServiceInterface
//
//  Created by Nicolas Lasso on 11/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit

public class AnyFeature {
    public let build: (StoreInterface, Route?) -> UIViewController

    public init<T: Feature>(_ feature: T.Type) {
        build = { store, route in
            // swiftlint:disable:next force_cast
            let initializer = feature.dependenciesInitializer
            let dependencies = initializer.build(store) as! T.Dependencies
            return feature.build(
                dependencies: dependencies,
                fromRoute: route
            )
        }
    }
}
