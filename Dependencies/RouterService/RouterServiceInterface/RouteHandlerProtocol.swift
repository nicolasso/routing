//
//  RouteHandlerProtocol.swift
//  RouterServiceInterface
//
//  Created by Nicolas Lasso on 10/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation

public protocol Route {
    static var identifier: String { get }
}

public protocol RouteHandler {
    
    var routes: [Route.Type] { get }
    
    func destination(of route: Route) -> AnyFeature
    
}
