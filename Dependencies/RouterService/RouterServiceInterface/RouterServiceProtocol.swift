//
//  RouterServiceProtocol.swift
//  RouterServiceInterface
//
//  Created by Nicolas Lasso on 10/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit

public protocol RouterServiceProtocol {
    
    func navigate(
        toRoute route: Route,
        fromView viewController: UIViewController
    )
    
}
