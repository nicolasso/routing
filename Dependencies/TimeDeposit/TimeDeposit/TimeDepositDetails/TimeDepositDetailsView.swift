//
//  TimeDepositDetailsView.swift
//  TimeDeposit
//
//  Created by Nicolas Lasso on 08/07/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import Foundation
import UIKit

class TimeDepositDetailsView: UIView {
    
    let title = prepareTitle()
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = UIColor.white
        self.addSubviews()
        self.setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func addSubviews() {
        self.addSubview(title)
    }
    
    fileprivate func setConstraints() {
        NSLayoutConstraint.activate([
            title.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            title.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            title.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
        ])
    }
    
}

private func prepareTitle() -> UILabel {
    let title = UILabel()
    title.translatesAutoresizingMaskIntoConstraints = false
    title.textColor = UIColor.black
    title.font = UIFont.systemFont(ofSize: 16)
    
    return title
}
