//
//  AppDelegate.swift
//  RoutingTest
//
//  Created by Nicolas Lasso on 02/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import UIKit

import Analysis
import Loans

import AnalysisRoutesInterface
import LoansRoutesInterface

import RouterServiceInterface
import RouterService

//TODO: Add TimeDeposit Interfaces.

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var routerService: RouterService!
    var window: UIWindow?
    var navigationController: UINavigationController!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        routerService = RouterService()
        
        //TODO: Register Service.
        
        self.routerService.register(dependencyFactory: {
            return LoanService()
        }, forType: LoanServiceProtocol.self)
        
        //TODO: Register Time Deposit RouteHandler.
        self.routerService.register(routeHandler: AnalysisRouteHandler())
        self.routerService.register(routeHandler: LoansRouteHandler())
        
        navigationController = routerService.navigationController(withInitialFeature: AnalysisHome.self)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        return true
    }

}

