//
//  ViewController.swift
//  RoutingTest
//
//  Created by Nicolas Lasso on 02/06/2020.
//  Copyright © 2020 Brubank. All rights reserved.
//

import UIKit
import RouterServiceInterface

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
    }

}

